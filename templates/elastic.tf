module "foundation_elasticsearch" {
  source  = "../modules/elasticsearch"
  master_node_count=1
  data_node_count=1
  name_prefix="elastic"
  master_node_snapshot_ids = ["snap-0631d1548f6bfb48e"]
  data_node_snapshot_ids = ["snap-0631d1548f6bfb48e"]  
  vpc_id = "${module.vpc.default_vpc_id}"
  logstash_beats_address=""
  node_ami = "${data.aws_ami.ubuntu.id}"
  key_name="${module.vpc.default_vpc_id}" 
  private_subnet_ids="${module.vpc.private_subnets}"
  public_subnet_ids="${module.vpc.public_subnets}"  
  elasticsearch_dns_name=""
  deploy_curator="true"
  credstash_kms_key_arn="${module.credstash.kms_key_arn}"
  credstash_reader_policy_arn="${module.credstash.reader_policy_arn}"
  credstash_install_snippet="${module.credstash.install_snippet}"
  credstash_get_cmd="${module.credstash.get_cmd}" 
  internal_alb={
    "security_group_id"="${aws_security_group.elastic_security.id}",
    "dns_name"=""
  }
  external_alb={
    "certificate_arn"="${aws_acm_certificate.cert.arn}",
    "deploy_elb"="true",
    "security_group_id"="${aws_security_group.elastic_security.id}"
  } 
  external_alb_setup="true"
  elasticsearch_version="6.3"

}