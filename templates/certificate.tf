resource "tls_private_key" "elasticdemo" {
  algorithm = "RSA"
}

resource "tls_self_signed_cert" "elasticdemo" {
  key_algorithm   = "RSA"
  private_key_pem = "${tls_private_key.elasticdemo.private_key_pem}"

  subject {
    common_name  = "elasticdemo.com"
    organization = "ACME elasticdemos, Inc"
  }

  validity_period_hours = 24

  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
  ]
}

resource "aws_acm_certificate" "cert" {
  private_key      = "${tls_private_key.elasticdemo.private_key_pem}"
  certificate_body = "${tls_self_signed_cert.elasticdemo.cert_pem}"
}