resource "aws_security_group" "elastic_security" {
  name = "elastic_security"
  description = "All all elasticsearch traffic"
  vpc_id = "${module.vpc.default_vpc_id}"

  # elasticsearch port
  ingress {
    from_port   = 9200
    to_port     = 9200
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # # logstash port
  # ingress {
  #   from_port   = 5043
  #   to_port     = 5044
  #   protocol    = "tcp"
  #   cidr_blocks = ["0.0.0.0/0"]
  # }

  # # kibana ports
  # ingress {
  #   from_port   = 5601
  #   to_port     = 5601
  #   protocol    = "tcp"
  #   cidr_blocks = ["0.0.0.0/0"]
  # }

  # ssh
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
}