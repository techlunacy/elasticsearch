module "credstash" {
  source               = "fpco/foundation/aws//modules/credstash-setup"
  create_reader_policy = true # can be ommitted if secrets are writeonly from within EC2
  create_writer_policy = true # can be ommitted if secrets are readonly from within EC2
}
#